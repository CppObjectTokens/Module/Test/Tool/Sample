/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Sample/Literal.h>

namespace Test
{

namespace Sample
{

template <class Value>
class Base
{
public:
    Base() : m_base_value{} {}
    explicit Base(const Value& value) : m_base_value{value} {}

    bool operator==(const Base& other) const
    { return (*this).m_base_value == other.m_base_value; }

    bool operator==(const Value& value) const
    { return (*this).m_base_value == value; }

private:
    Value m_base_value;
};

} // namespace Sample

template <class Value>
struct LiteralOf<Sample::Base<Value>>
{
    using Type = typename LiteralOf<std::decay_t<Value>>::Type;
};

template <class Value>
struct LiteralHolder<Sample::Base<Value>>
    : public LiteralHolder<typename std::decay<Value>::type>
{};

} // namespace Test
