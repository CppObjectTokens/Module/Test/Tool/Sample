/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Sample/Literal.h>

namespace Test
{

namespace Sample
{

class NonCopyable
{
public:
    ~NonCopyable() = default;
    NonCopyable()  = default;

    explicit NonCopyable(int value) noexcept : m_value{value} {}

    NonCopyable(const NonCopyable& other) = delete;
    NonCopyable(NonCopyable&& other)      = default;

    NonCopyable& operator=(const NonCopyable& other) = delete;
    NonCopyable& operator=(NonCopyable&& other)      = default;

    bool operator==(const NonCopyable& other) const noexcept
    { return (*this).m_value == other.m_value; }

    bool operator==(int value) const noexcept
    { return (*this).m_value == value; }

private:
    int m_value{};
};

} // namespace Sample

template <>
struct LiteralOf<Sample::NonCopyable>
{
    using Type = int;
};

template <>
constexpr LiteralHolder<Sample::NonCopyable>::Literal
LiteralHolder<Sample::NonCopyable>::literal()
{ return 7; }

} // namespace Test
