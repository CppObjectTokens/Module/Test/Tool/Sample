/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Sample/Abstract.h>
#include <Test/Sample/Conform.h>

namespace Test
{

namespace Sample
{

template <class Value_>
class Concrete : public Abstract<Value_>
{
private:
    using ParentType = Abstract<Value_>;

public:
    using typename ParentType::Value;
    using ParentType::operator==;

    Concrete() : m_value{} {}
    explicit Concrete(const Value& value) : m_value{value} {}

    bool operator==(const ParentType& other) const
    { return m_value == other.value(); }

private:
    Value&       value() override { return m_value; }
    const Value& value() const override { return m_value; }
    void  setValue(const Value& value) override { m_value = value; }

    Value m_value;
};

template <class Value_>
class Concrete<const Value_> : public Abstract<const Value_>
{
private:
    using ParentType = Abstract<const Value_>;

public:
    using typename ParentType::Value;
    using ParentType::operator==;

    Concrete() : m_value{} {}
    Concrete(const Value& value) : m_value{value} {}

    bool operator==(const ParentType& other) const
    { return m_value == other.value(); }

private:
    const Value& value() const override { return m_value; }

    Value m_value;
};

} // namespace Sample

template <class Value_>
struct LiteralOf<Sample::Concrete<Value_>>
{
    using Type = typename LiteralOf<std::decay_t<Value_>>::Type;
};

template <class Value_>
struct LiteralHolder<Sample::Concrete<Value_>>
    : public LiteralHolder<typename std::decay<Value_>::type>
{};

namespace Conform
{

namespace Internal
{

template <class Value_>
struct ConcreteOf<Sample::Abstract<Value_>>
{
    using Type = Sample::Concrete<Value_>;
};

template <class Value_>
struct ConcreteOf<const Sample::Abstract<Value_>>
{
    using Type = const Sample::Concrete<Value_>;
};

} // namespace Internal

} // namespace Conform

} // namespace Test
