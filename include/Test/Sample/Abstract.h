/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Sample/Literal.h>

namespace Test
{

namespace Sample
{

template <class Value_>
class Abstract
{
public:
    using Value = Value_;

    virtual ~Abstract() {}

    virtual Value&       value()              = 0;
    virtual const Value& value() const        = 0;
    virtual void setValue(const Value& value) = 0;

    bool operator==(const Abstract& other) const
    { return (*this).value() == other.value(); }

    bool operator==(const Value& value) const
    { return (*this).value() == value; }
};

template <class Value_>
class Abstract<const Value_>
{
public:
    using Value = const Value_;

    virtual ~Abstract() {}

    virtual const Value& value() const = 0;

    bool operator==(const Abstract& other) const
    { return (*this).value() == other.value(); }

    bool operator==(const Value& value) const
    { return (*this).value() == value; }
};

} // namespace Sample

template <class Value_>
struct LiteralOf<Sample::Abstract<Value_>>
{
    using Type = typename LiteralOf<std::decay_t<Value_>>::Type;
};

template <class Value_>
struct LiteralHolder<Sample::Abstract<Value_>>
    : public LiteralHolder<typename std::decay<Value_>::type>
{};

} // namespace Test
