/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Sample/Base.h>
#include <Test/Sample/Conform.h>

namespace Test
{

namespace Sample
{

template <class Value>
struct Derived : Base<Value>
{
    Derived() : Base<Value>{}, m_derived_value{} {}
    explicit Derived(const Value& value)
        : Base<Value>{value}, m_derived_value{value}
    {}

    bool operator==(const Base<Value>& other) const
    { return Base<Value>::operator==(other); }

    bool operator==(const Derived& other) const
    {
        return Base<Value>::operator==(other)
               && (*this).m_derived_value == other.m_derived_value;
    }

    bool operator==(const Value& value) const
    {
        return Base<Value>::operator==(value)
               && (*this).m_derived_value == value;
    }

private:
    Value m_derived_value;
};

} // namespace Sample

template <class Value>
struct LiteralOf<Sample::Derived<Value>>
{
    using Type = typename LiteralOf<std::decay_t<Value>>::Type;
};

template <class Value>
struct LiteralHolder<Sample::Derived<Value>>
    : public LiteralHolder<typename std::decay<Value>::type>
{};

} // namespace Test
